/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <DD4hep/DetFactoryHelper.h>

using namespace std;
using namespace dd4hep;

// return Volume or Assembly?
dd4hep::Volume create_cell_half( Detector& description, xml_h e ) {

  xml_det_t x_det = e;

  // Assembly for a generic left/right half
  Assembly half_vol( "CellHalf" ); // make this a volume?

  xml_dim_t x_tube = x_det.child( _U( tube ) );
  xml_dim_t x_cone = x_det.child( _U( cone ) );
  xml_dim_t x_box  = x_det.child( _U( box ) );

  // main body: a cylindrical aluminium tube
  Tube   tube( x_tube.rmin(), x_tube.rmin() + x_tube.thickness(), x_tube.length() / 2, -M_PI / 2, M_PI / 2 );
  Volume tube_vol( "CellTube", tube, description.material( "Al" ) );
  // rectangular wings + terminal support
  float wing_angle = -M_PI / 4; // wing angle wrt y axis
  float wing_x     = +x_box.dy() / 2 * cos( wing_angle ) - x_box.dx() / 2 * cos( wing_angle );
  float wing_y =
      -x_tube.rmin() - x_tube.thickness() + x_box.dy() / 2 * sin( wing_angle ) + x_box.dx() / 2 * sin( wing_angle );
  float wing_z = 0;
  // wing position
  Position wing_pos( wing_x, wing_y, wing_z );
  Position wing2_pos = wing_pos + Position( 0, x_box.dy(), 0 );
  float    wing_delta =
      dd4hep::_toDouble( "SMOG2:wing_delta" ); // this is the amount by which the wing is inserted into the tube
  Position wing_pos_int( wing_x + wing_delta * cos( std::fabs( wing_angle ) ),
                         wing_y + wing_delta * sin( std::fabs( wing_angle ) ), wing_z );
  Position wing2_pos_int = wing_pos_int + Position( 0, x_box.dy(), 0 );

  // wing
  Box wing_box( x_box.dx() / 2 + wing_delta, x_box.dy() / 2, x_box.dz() / 2 );
  // wing support
  float supp_h = dd4hep::_toDouble( "SMOG2:supp_h" );
  Box   wing_supp( x_box.dx() / 2 + wing_delta, supp_h / 2, x_box.dy() / 2 );
  // wing + support
  UnionSolid wing_box_wsupp( wing_box, wing_supp,
                             Position( 0, -supp_h / 2 + x_box.dy() / 2, -x_box.dz() / 2 - x_box.dy() / 2 ) );
  // main body: tube + wing + support
  UnionSolid wing_full( tube, wing_box_wsupp, Transform3D( RotationZ( -wing_angle ), wing_pos_int ) );
  wing_full = UnionSolid( wing_full, wing_box_wsupp, Transform3D( RotationZ( -wing_angle ), -wing2_pos_int ) );
  // subtracting the inner tube
  Tube             tube_int_sub( 0, x_tube.rmin(), 1.2 * x_tube.length() / 2, 0, 2 * M_PI );
  SubtractionSolid wing( wing_full, tube_int_sub );
  // circular part that joins the two wing supports
  Position tube_supp_pos( 0, 0, -x_tube.length() / 2 - x_box.dy() / 2 );
  // 0.87 to make the transition from wing supp to circ supp
  Tube tube_supp_full( x_tube.rmin(), x_tube.rmin() + supp_h * 0.87, 0.9 * x_box.dy() / 2, -M_PI / 2, M_PI / 4 );
  SubtractionSolid tube_supp( tube_supp_full, wing, -tube_supp_pos );
  Volume           tube_supp_vol( "TubeSupp", tube_supp, description.material( "Al" ) );
  tube_supp_vol.setVisAttributes( description, "SMOG2:WingVis" );
  half_vol.placeVolume( tube_supp_vol, tube_supp_pos );

  // This shape is used to subtract any material under the wings
  float      h_sub = dd4hep::_toDouble( "SMOG2:h_sub" );
  Box        wing_supp_sub( x_box.dx() / 2, h_sub / 2, x_tube.length() );
  UnionSolid wing_sub( wing_box, wing_supp_sub, Position( 0, +h_sub / 2 - x_box.dy() / 2, 0 ) );
  // this position clears everything below the wing top surface:
  Position wing_sub_pos( wing_x - ( h_sub + x_box.dy() ) / 2 * cos( wing_angle ),
                         wing_y - ( h_sub + x_box.dy() ) / 2 * sin( wing_angle ), wing_z );
  // this position clears everything below the wing bottom surface:
  Position wing_sub_pos_b( -wing_x - ( h_sub + x_box.dy() ) / 2 * cos( wing_angle ),
                           -wing_y - ( h_sub + x_box.dy() ) / 2 * sin( wing_angle ), wing_z );
  Position wing2_sub_pos_b( -wing_x - ( h_sub + x_box.dy() ) / 2 * cos( wing_angle ),
                            -wing_y - ( h_sub + x_box.dy() ) / 2 * sin( wing_angle ) - x_box.dy(), wing_z );

  // 2x circular supports holding each wing
  float supp_dist      = dd4hep::_toDouble( "SMOG2:supp_dist" ); // distance from the tube end to the circular supports
  float supp_rmin      = dd4hep::_toDouble( "SMOG2:supp_rmin" );
  float supp_rmax      = dd4hep::_toDouble( "SMOG2:supp_rmax" );
  float supp_thickness = dd4hep::_toDouble( "SMOG2:supp_thickness" );
  Tube  supp_ring_full( supp_rmin, supp_rmax, supp_thickness / 2, -M_PI, 2 * 0.9 * std::fabs( wing_angle ) );
  SubtractionSolid supp_ring( supp_ring_full, wing_sub, Transform3D( RotationZ( -wing_angle ), wing_pos ) );
  supp_ring = SubtractionSolid( supp_ring, wing_sub, Transform3D( RotationZ( -wing_angle ), -wing_pos ) );

  SubtractionSolid supp( supp_ring, wing );
  Volume           supp_vol( "CellSupp", supp, description.material( "Al" ) );
  supp_vol.setVisAttributes( description, "SMOG2:SuppVis" );
  half_vol.placeVolume( supp_vol, Position( 0, 0, x_tube.length() / 2 - supp_dist ) );
  half_vol.placeVolume( supp_vol, Position( 0, 0, -x_tube.length() / 2 + supp_dist ) );

  Volume wing_vol( "Wing", wing, description.material( "Al" ) );
  wing_vol.setVisAttributes( description, "SMOG2:WingVis" );
  half_vol.placeVolume( wing_vol );

  // cone attached to the end of the tube
  Position         cone_pos( 0, 0, x_tube.length() / 2 + x_cone.length() / 2 );
  ConeSegment      outcone_full( x_cone.length() / 2, x_tube.rmin(), x_tube.rmin() + x_tube.thickness(), x_cone.rmax(),
                            x_cone.rmax() + x_tube.thickness(), -M_PI, 2 * 0.9 * std::fabs( wing_angle ) );
  SubtractionSolid outcone( outcone_full, wing_supp_sub, Transform3D( RotationZ( -wing_angle ), wing_sub_pos ) );
  outcone = SubtractionSolid( outcone, wing_supp_sub, Transform3D( RotationZ( -wing_angle ), wing_sub_pos_b ) );
  outcone = SubtractionSolid( outcone, wing_supp_sub, Transform3D( RotationZ( -wing_angle ), wing2_sub_pos_b ) );
  // triangular wings attached to the cone
  float      delta_z = dd4hep::_toDouble( "SMOG2:delta_z" ); // tolerance to avoid leftover material
  Box        wingt_box( x_box.dx() / 2, x_box.dy() / 2, x_cone.length() / 2 - delta_z / 2 );
  UnionSolid wingt_full( outcone, wingt_box,
                         Transform3D( RotationZ( -wing_angle ), wing_pos - Position( 0, 0, delta_z / 2 ) ) );
  wingt_full = UnionSolid( wingt_full, wingt_box,
                           Transform3D( RotationZ( -wing_angle ), -wing2_pos - Position( 0, 0, delta_z / 2 ) ) );
  ConeSegment      intcone_sub( x_cone.length() / 2, 0, x_tube.rmin(), 0, x_cone.rmax(), 0, 2 * M_PI );
  SubtractionSolid wingt( wingt_full, intcone_sub );
  Volume           wingt_vol( "WingT", wingt, description.material( "Al" ) );
  wingt_vol.setVisAttributes( description, "SMOG2:WingTVis" );
  half_vol.placeVolume( wingt_vol, Position( 0, 0, ( x_box.dz() + x_cone.length() ) / 2 ) );

  // openable flange ring connecting the cone to the upstream WFS
  float ring_dr = dd4hep::_toDouble( "SMOG2:ring_dr" ); // difference between outer and inner radii of the rings
  Tube  ring_full( x_cone.rmax(), x_cone.rmax() + ring_dr, x_box.dy() / 2, -M_PI, 2 * 0.9 * std::fabs( wing_angle ) );
  SubtractionSolid ring( ring_full, wing_sub, Transform3D( RotationZ( -wing_angle ), wing_pos ) );
  ring = SubtractionSolid( ring, wing_sub, Transform3D( RotationZ( -wing_angle ), -wing2_pos ) );
  Volume ring_vol( "CellRing", ring, description.material( "Al" ) );
  ring_vol.setVisAttributes( description, "SMOG2:SuppVis" );
  Position ring_pos( 0, 0, x_tube.length() / 2 + x_cone.length() + x_box.dy() / 2 );
  half_vol.placeVolume( ring_vol, ring_pos );

  // pistons (1 small + 1 big) attached to the circular supports
  float       pistS_r      = dd4hep::_toDouble( "SMOG2:pistS_r" );
  float       pistS_length = dd4hep::_toDouble( "SMOG2:pistS_length" );
  float       pistB_r      = dd4hep::_toDouble( "SMOG2:pistB_r" );
  float       pistB_length = dd4hep::_toDouble( "SMOG2:pistB_length" );
  Tube        pistSp( 0, pistS_r, pistS_length / 2, 0, 2 * M_PI );
  Tube        pistBp( 0, pistB_r, pistB_length / 2, 0, 2 * M_PI );
  UnionSolid  pistS( pistSp, pistBp, Position( 0, 0, ( pistS_length + pistB_length ) / 2 ) );
  RotationZYX pist_rot( 0, M_PI / 2, 0 );
  Position    pist_pos( supp_rmax + pistS_length / 2, 0, 0 ); // add a clearance?
  Volume      pist_vol( "CellPist", pistS, description.material( "Al" ) );
  pist_vol.setVisAttributes( description, "SMOG2:RingVis" );

  half_vol.placeVolume( pist_vol,
                        Transform3D( pist_rot, pist_pos + Position( 0, 0, x_tube.length() / 2 - supp_dist ) ) );
  RotationZYX pist_rot2( 0, M_PI / 2, M_PI );
  half_vol.placeVolume( pist_vol,
                        Transform3D( pist_rot2, pist_pos + Position( 0, 0, -x_tube.length() / 2 + supp_dist ) ) );

  // lateral support bar joining the two pistons
  float bar_length  = x_tube.length() - 2 * supp_dist;
  float bar_dim     = dd4hep::_toDouble( "SMOG2:bar_dim" );
  float hole_length = bar_length / 8;
  float hole_delta  = bar_dim;

  Box              tbar_full( bar_dim / 2, bar_dim / 2, bar_length / 2 );
  Box              tbar_hole( 1.1 * bar_dim / 2, 0.6 * bar_dim / 2, hole_length / 2 );
  Position         hole1_pos( 0, 0, bar_length / 2 - hole_length / 2 - hole_delta );
  Position         hole2_pos = hole1_pos - Position( 0, 0, hole_length + hole_delta );
  SubtractionSolid tbar( tbar_full, tbar_hole, hole1_pos );
  tbar = SubtractionSolid( tbar, tbar_hole, hole2_pos );
  tbar = SubtractionSolid( tbar, tbar_hole, -hole1_pos );
  tbar = SubtractionSolid( tbar, tbar_hole, -hole2_pos );
  Position tbar_pos( supp_rmax + pistS_length + pistB_length + bar_dim / 2, 0, 0 );
  Volume   tbar_vol( "CellTbar", tbar, description.material( "Al" ) );
  tbar_vol.setVisAttributes( description, "SMOG2:SuppVis" );
  half_vol.placeVolume( tbar_vol, tbar_pos );

  // downstream WFS
  float wfs_supp_h      = 2 * supp_h;
  float wfs_down_length = dd4hep::_toDouble( "SMOG2:wfs_down_length" );

  Box        wfs_supp( x_box.dx() / 2 + wing_delta, wfs_supp_h / 2, x_box.dy() / 2 );
  Box        wfs_down_box( x_box.dx() / 2 + wing_delta, x_box.dy() / 2, wfs_down_length / 2 - delta_z / 2 );
  UnionSolid wfs_down_wsupp(
      wfs_down_box, wfs_supp,
      Position( 0, -wfs_supp_h / 2 + x_box.dy() / 2, -wfs_down_length / 2 - delta_z / 2 - x_box.dy() / 2 ) );

  float            rmin      = x_tube.rmin() + x_tube.thickness();
  float            rmax      = rmin + ring_dr;
  float            wfs_thick = x_tube.thickness();
  ConeSegment      wfs_down_cone_full( wfs_down_length / 2, rmax, rmax + wfs_thick, rmin, rmin + wfs_thick, -M_PI,
                                  2 * 0.9 * std::fabs( wing_angle ) );
  SubtractionSolid wfs_down_cone( wfs_down_cone_full, wing_supp_sub,
                                  Transform3D( RotationZ( -wing_angle ), wing_sub_pos ) );
  wfs_down_cone =
      SubtractionSolid( wfs_down_cone, wing_supp_sub, Transform3D( RotationZ( -wing_angle ), wing_sub_pos_b ) );
  wfs_down_cone =
      SubtractionSolid( wfs_down_cone, wing_supp_sub, Transform3D( RotationZ( -wing_angle ), wing2_sub_pos_b ) );
  float wfs_clear =
      dd4hep::_toDouble( "SMOG2:wfs_clear" ); // horizontal difference btw wing length and upstream WFS length
  Position wfs_supp_pos =
      wing_pos_int + Position( wfs_clear * cos( std::fabs( wing_angle ) ), wfs_clear * sin( std::fabs( wing_angle ) ),
                               0 ); // i.e. the WFS has the same position of the wing but it's inserted into the
                                    // cylinder by wfs_clear to make it shorter on one side
  UnionSolid wfs_down_full( wfs_down_cone, wfs_down_wsupp, Transform3D( RotationZ( -wing_angle ), wfs_supp_pos ) );
  wfs_down_full = UnionSolid( wfs_down_full, wfs_down_wsupp, Transform3D( RotationZ( -wing_angle ), -wing2_pos_int ) );
  // circular support
  Position wfs_tube_supp_pos = tube_supp_pos - Position( 0, 0, wfs_down_length + x_box.dy() );
  // 0.7 to make the transition from wing supp to circ supp
  Tube             wfs_tube_supp_full( rmax, rmax + wfs_supp_h * 0.7, 0.9 * x_box.dy() / 2, -M_PI / 2, M_PI / 4 );
  SubtractionSolid wfs_tube_supp( wfs_tube_supp_full, wfs_down_full,
                                  Position( 0, 0, +wfs_down_length / 2 + x_box.dy() / 2 ) );
  Volume           wfs_tube_supp_vol( "WFSTubeSupp", wfs_tube_supp, description.material( "Al" ) );
  wfs_tube_supp_vol.setVisAttributes( description, "SMOG2:WFSVis" );
  half_vol.placeVolume( wfs_tube_supp_vol, wfs_tube_supp_pos );
  // cone + Tube to subtract the inner material
  ConeSegment      cone_int_sub( wfs_down_length / 2, 0, rmax, 0, rmin, 0, 2 * M_PI );
  Tube             cone_end( 0, rmax, wfs_down_length / 2, 0, 2 * M_PI );
  UnionSolid       cone_int_sub2( cone_int_sub, cone_end, Position( 0, 0, -wfs_down_length ) );
  SubtractionSolid wfs_down_temp( wfs_down_full, cone_int_sub2 );
  // adding holes
  float wfs_hole_r = dd4hep::_toDouble( "SMOG2:wfs_hole_r" );
  Tube  wfs_hole( 0, wfs_hole_r, supp_h, 0, 2 * M_PI );

  // hole positions along the wing coordinate
  float h1 = x_box.dx() / 2 - rmax - wfs_hole_r;
  float h2 = x_box.dx() / 2;

  Position wfs_hole_pos1 = wing_pos_int +
                           Position( h1 * cos( std::fabs( wing_angle ) ), h1 * sin( std::fabs( wing_angle ) ), 0 ) +
                           Position( supp_h, 0, 0 );
  SubtractionSolid wfs_down( wfs_down_temp, wfs_hole, wfs_hole_pos1 );

  Position wfs_hole_pos2 = wing_pos_int -
                           Position( -wfs_clear + h2 * cos( std::fabs( wing_angle ) ),
                                     -wfs_clear + h2 * sin( std::fabs( wing_angle ) ), 0 ) +
                           Position( supp_h, 0, 0 );
  wfs_down = SubtractionSolid( wfs_down, wfs_hole, wfs_hole_pos2 );

  Volume wfs_down_vol( "WFSVol", wfs_down, description.material( "Al" ) );
  wfs_down_vol.setVisAttributes( description, "SMOG2:WFSVis" );
  Position wfs_down_pos( 0, 0, -x_tube.length() / 2 - x_box.dy() - wfs_down_length / 2 );
  half_vol.placeVolume( wfs_down_vol, wfs_down_pos );

  return half_vol;
}

// parts which are not split into two halves

dd4hep::Volume create_cell_fixed( Detector& description, xml_h e ) {

  xml_det_t x_det   = e;
  float     ring_dr = dd4hep::_toDouble( "SMOG2:ring_dr" ); // difference between outer and inner radii of the rings

  Assembly fixed_vol( "CellFixed" ); // make this a volume?

  xml_dim_t x_cone = x_det.child( _U( cone ) );
  xml_dim_t x_box  = x_det.child( _U( box ) );

  float wfs_length = dd4hep::_toDouble( "SMOG2:wfs_length" );

  // connecting WFS flange
  Tube   wfs_ring( x_cone.rmax(), x_cone.rmax() + ring_dr, x_box.dy() / 2, 0, 2 * M_PI );
  Volume wfs_ring_vol( "CellWfs_Ring", wfs_ring, description.material( "Al" ) );
  wfs_ring_vol.setVisAttributes( description, "SMOG2:RingVis" );
  Position wfs_ring_pos( 0, 0, x_box.dy() / 2 );
  fixed_vol.placeVolume( wfs_ring_vol, wfs_ring_pos );

  // upstream WFS
  Position wfs_pos     = wfs_ring_pos + Position( 0, 0, x_box.dy() / 2 + wfs_length / 2 );
  int      nstrips     = 34;
  float    angle_max   = 2 * M_PI;
  float    angle_delta = angle_max / ( 2 * nstrips );

  for ( float i = 0; i < 2 * nstrips; i += 2 ) {
    float  wfs_angle_min = -M_PI + i * angle_delta;
    float  wfs_angle_max = -M_PI + ( i + 1 ) * angle_delta;
    Tube   wfs( x_cone.rmax(), x_cone.rmax() + x_box.dy(), wfs_length / 2, wfs_angle_min, wfs_angle_max );
    Volume wfs_vol( "CellWFS", wfs, description.material( "Al" ) );
    wfs_vol.setVisAttributes( description, "SMOG2:WFSVis" );
    fixed_vol.placeVolume( wfs_vol, wfs_pos );
  }

  // upstream WFS flange
  float  supp_thickness = dd4hep::_toDouble( "SMOG2:supp_thickness" ); // define this outside?
  Tube   flange( x_cone.rmax(), x_cone.rmax() + ring_dr, supp_thickness / 2, 0,
               2 * M_PI ); // flange thickness = wing thickness
  Volume flange_vol( "CellFlange", flange, description.material( "Al" ) );
  flange_vol.setVisAttributes( description, "SMOG2:RingVis" );
  Position flange_pos = wfs_pos + Position( 0, 0, wfs_length / 2 + supp_thickness / 2 );

  fixed_vol.placeVolume( flange_vol, flange_pos );

  return fixed_vol;
}

static Ref_t create_cell_left( Detector& description, xml_h e, Ref_t ) {

  xml_det_t  x_det    = e;
  string     det_name = x_det.nameStr();
  DetElement cell_det( det_name, x_det.id() );

  dd4hep::Volume half_vol = create_cell_half( description, e );

  Volume    motherVol = description.pickMotherVolume( cell_det );
  xml_dim_t pos       = x_det.child( _U( position ) );
  xml_dim_t rot       = x_det.child( _U( rotation ) );
  // add opening shift here?
  PlacedVolume pv = motherVol.placeVolume( half_vol, Transform3D( RotationZYX( rot.z( 0 ), rot.y( 0 ), rot.x( 0 ) ),
                                                                  Position( pos.x( 0 ), pos.y( 0 ), pos.z( 0 ) ) ) );
  cell_det.setPlacement( pv );

  return cell_det;
}
DECLARE_DETELEMENT( LHCb_SMOG2_Left_v1_0, create_cell_left )

static Ref_t create_cell_right( Detector& description, xml_h e, Ref_t ) {

  xml_det_t      x_det    = e;
  string         det_name = x_det.nameStr();
  DetElement     cell_det( det_name, x_det.id() );
  dd4hep::Volume half_vol = create_cell_half( description, e );

  Volume    motherVol = description.pickMotherVolume( cell_det );
  xml_dim_t pos       = x_det.child( _U( position ) );
  xml_dim_t rot       = x_det.child( _U( rotation ) );

  PlacedVolume pv = motherVol.placeVolume( half_vol, Transform3D( RotationZYX( rot.z( 0 ), rot.y( 0 ), rot.x( 0 ) ),
                                                                  Position( pos.x( 0 ), pos.y( 0 ), pos.z( 0 ) ) ) );
  cell_det.setPlacement( pv );

  return cell_det;
}
DECLARE_DETELEMENT( LHCb_SMOG2_Right_v1_0, create_cell_right )

static Ref_t create_cell_fixed( Detector& description, xml_h e, Ref_t ) {

  xml_det_t      x_det    = e;
  string         det_name = x_det.nameStr();
  DetElement     cell_det( det_name, x_det.id() );
  dd4hep::Volume fixed_vol = create_cell_fixed( description, e );

  Volume    motherVol = description.pickMotherVolume( cell_det );
  xml_dim_t pos       = x_det.child( _U( position ) );
  xml_dim_t rot       = x_det.child( _U( rotation ) );

  PlacedVolume pv = motherVol.placeVolume( fixed_vol, Transform3D( RotationZYX( rot.z( 0 ), rot.y( 0 ), rot.x( 0 ) ),
                                                                   Position( pos.x( 0 ), pos.y( 0 ), pos.z( 0 ) ) ) );
  cell_det.setPlacement( pv );

  return cell_det;
}
DECLARE_DETELEMENT( LHCb_SMOG2_Fixed_v1_0, create_cell_fixed )
