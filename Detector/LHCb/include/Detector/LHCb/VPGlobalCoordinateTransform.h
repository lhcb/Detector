/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <nlohmann/json_fwd.hpp>

#include <Math/RotationZYX.h>
#include <Math/Translation3D.h>

/**
 * This condition is only present for the offline calibration.
 */
namespace LHCb::Detector {
  struct VPGlobalCoordinateTransform {

    VPGlobalCoordinateTransform() = default;
    VPGlobalCoordinateTransform( const nlohmann::json& obj );

    ROOT::Math::Translation3D oldPosition;
    ROOT::Math::RotationZYX   oldRotation;

    bool enabled;
  };
} // namespace LHCb::Detector
