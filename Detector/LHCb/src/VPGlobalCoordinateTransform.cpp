/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Detector/LHCb/VPGlobalCoordinateTransform.h>

#include <Core/Utils.h>

#include <DD4hep/DD4hepUnits.h>
#include <DD4hep/Handle.h>

#include <nlohmann/json.hpp>

template <std::size_t Size>
std::array<double, Size> arrayFromJson( const nlohmann::json& jsonArray, const double unit ) {
  if ( jsonArray.size() != Size ) { throw std::runtime_error{"Condition array is not of expected size"}; }

  std::array<double, Size> values;

  for ( std::size_t i{0}; i < Size; ++i ) {
    if ( jsonArray[i].is_string() ) {
      values[i] = dd4hep::_toDouble( jsonArray[i].get<std::string>() );
    } else if ( jsonArray[i].is_number() ) {
      values[i] = jsonArray[i].get<double>() * unit;
    } else {
      throw std::runtime_error{"Condition contains unexpected types"};
    }
  }

  return values;
}

LHCb::Detector::VPGlobalCoordinateTransform::VPGlobalCoordinateTransform( const nlohmann::json& obj ) {
  if ( !obj.contains( "oldPosition" ) || !obj.contains( "oldRotation" ) ) {
    throw std::runtime_error{
        "Could not find old position & rotation information - are you running the latest version of the software?"};
  }

  if ( !obj.contains( "newPosition" ) || !obj.contains( "newRotation" ) ) {
    throw std::runtime_error{"VPGlobalCoordinateTrransform condition is empty"};
  }

  constexpr std::size_t posSize{3};
  constexpr std::size_t rotationSize{3};

  auto&                       oldPos = obj.at( "oldPosition" );
  std::array<double, posSize> oldPosVals{arrayFromJson<posSize>( oldPos, dd4hep::mm )};

  auto&                            oldRotationData = obj.at( "oldRotation" );
  std::array<double, rotationSize> oldRotationVals{arrayFromJson<rotationSize>( oldRotationData, dd4hep::radian )};

  oldPosition.SetComponents( oldPosVals.begin(), oldPosVals.end() );

  // The order in the yaml is RX RY RZ - mind the 'rbegin' and 'rend'!
  oldRotation.SetComponents( oldRotationVals.rbegin(), oldRotationVals.rend() );

  const auto& read_enabled_flag = obj.at( "enabled" );

  // support both 'true/false' and 1 / 0
  enabled = read_enabled_flag.is_boolean() ? read_enabled_flag.get<bool>() : bool( read_enabled_flag.get<int>() );
}
