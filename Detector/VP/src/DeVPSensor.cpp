/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/VP/DeVPSensor.h"
#include "Core/Keys.h"
#include "Core/PrintHelpers.h"
#include "DD4hep/Primitives.h"
#include "DD4hep/Printout.h"
#include <algorithm>
#include <nlohmann/json.hpp>

LHCb::Detector::detail::DeVPSensorObject::DeVPSensorObject( const dd4hep::DetElement&             de,
                                                            dd4hep::cond::ConditionUpdateContext& ctxt,
                                                            unsigned int moduleId, bool _isLeft )
    : DeIOVObject( de, ctxt, 1008106 ) // matches DetDesc CLID_VeloAlignCond
    , sensorNumber( VPChannelID::SensorID{de.id() + VP::NSensorsPerModule * moduleId} )
    , moduleNumber( moduleId )
    , isLeft( _isLeft ) {

  // get sensorHV, chipStatus and badPixel conditions
  // assemble sensor name in conditions : sensor_12_3 for module 12 sensor 3
  auto sensorCondName = fmt::format( "sensor_{:02d}_{:1d}", moduleNumber, de.id() );
  dd4hep::printout( dd4hep::DEBUG, "DeVPSensorObject",
                    "Reading condition " + sensorCondName + " to set params for " + de.path() );
  auto hashKey = LHCb::Detector::hash_key( de, sensorCondName );
  auto condsh  = ctxt.condition( hashKey, false );
  if ( condsh.isValid() ) {
    auto& node = condsh.get<nlohmann::json>();
    if ( !node.is_null() ) {
      sensorHV = node["SensorHV"].get<double>();
      if ( node["ChipStatus"].size() == 3 ) {
        for ( unsigned int iChip = 0; iChip < 3; ++iChip ) {
          chipStatusOK[iChip] = (bool)node["ChipStatus"][iChip].get<int>();
        }
      } else {
        dd4hep::printout( dd4hep::WARNING, "DeVPSensorObject",
                          "Failed to access YAML set chip status (3 values) of sensor " + sensorCondName );
      }
      // old name per pixel or new name per grid both do not cause error messages
      // Only BadPixelGrid removes pixels (BadPixelList was never used in production)
      if ( !node["BadPixelGrid"].is_null() || !node["BadPixelList"].is_null() ) {
        if ( node["BadPixelGrid"].is_array() ) { // skip an empty array (is_array == false)
          auto& gridList = node["BadPixelGrid"];
          badChannels.reserve( gridList.size() );
          for ( auto const& gridValues : gridList ) {
            if ( gridValues.size() != 3 ) {
              dd4hep::printout( dd4hep::WARNING, "DeVPSensorObject",
                                "Can not set BadPixelGrid as not three values in the vector" );
            } else {
              badChannels.emplace_back( gridValues[0].get<unsigned int>(), gridValues[1].get<unsigned int>(),
                                        gridValues[2].get<unsigned int>() );
            }
          }
          std::sort( badChannels.begin(), badChannels.end() );
        }
      } else {
        dd4hep::printout( dd4hep::WARNING, "DeVPSensorObject",
                          "Failed access YAML for bad pixel grid in " + sensorCondName );
      }
    } else {
      dd4hep::printout( dd4hep::DEBUG, "DeVPSensorObject", "YAML is null for " + sensorCondName + " " + de.path() );
    }
  } else {
    dd4hep::printout( dd4hep::DEBUG, "DeVPSensorObject", "Can not access YAML of sensor " + sensorCondName );
  }

  sizeX = nChips * chipSize + ( nChips - 1 ) * interChipDist;
  sizeY = chipSize;
  zpos  = toGlobal( ROOT::Math::XYZPoint{0.0, 0.0, 0.0} ).z();
  for ( unsigned int col = 0; col < VP::NSensorColumns; ++col ) {
    // Calculate the x-coordinate of the pixel centre and the pitch.
    double x0    = ( col / VP::NColumns ) * ( chipSize + interChipDist );
    double x     = x0 + ( col % VP::NColumns + 0.5 ) * pixelSize;
    double pitch = pixelSize;
    switch ( col ) {
    case 256:
    case 512:
      // right of chip border
      x -= 0.5 * ( interChipPixelSize - pixelSize );
      pitch = 0.5 * ( interChipPixelSize + pixelSize );
      break;
    case 255:
    case 511:
      // left of chip border
      x += 0.5 * ( interChipPixelSize - pixelSize );
      pitch = interChipPixelSize;
      break;
    case 254:
    case 510:
      // two left of chip border
      pitch = 0.5 * ( interChipPixelSize + pixelSize );
      break;
    }
    local_x[col] = x;
    x_pitch[col] = pitch;
  }
}

void LHCb::Detector::detail::DeVPSensorObject::print( int indent, int flg ) const {
  std::string prefix = getIndentation( indent );
  DeIOVObject::print( indent, flg );
  if ( flg & SPECIFIC ) {
    dd4hep::printout( dd4hep::INFO, "DeVPSensor", "%s+  >> Module:%d Sensor:%d %s %d Chips Rows:%d Cols:%d",
                      prefix.c_str(), moduleNumber, sensorNumber, isLeft ? "Left" : "Right", nChips, nCols, nRows );
  }
  if ( flg & DETAIL ) {
    dd4hep::printout( dd4hep::INFO, "DeVPSensor", "%s+  >> Thickness:%g ChipSize:%g Dist:%g Pix-Size:%g Dist:%g",
                      prefix.c_str(), thickness, chipSize, interChipDist, pixelSize, interChipPixelSize );
    dd4hep::printout( dd4hep::INFO, "DeVPSensor", "%s+  >> SizeX: %g SizeY: %g local:%ld pitch:%ld", prefix.c_str(),
                      sizeX, sizeY, local_x.size(), x_pitch.size() );
  }
}
