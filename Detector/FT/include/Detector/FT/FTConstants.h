/*****************************************************************************\
 * (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <array>
#include <cstdint>

#include <algorithm>
#include <climits>

// Creates a bit mask out of a number of allocated bits
#define BIT_MASK( __TYPE__, __ONE_COUNT__ )                                                                            \
  ( ( __TYPE__ )( -( ( __ONE_COUNT__ ) != 0 ) ) ) &                                                                    \
      ( ( (__TYPE__)-1 ) >> ( ( sizeof( __TYPE__ ) * CHAR_BIT ) - ( __ONE_COUNT__ ) ) )

namespace LHCb::Detector {
  namespace FT {

    enum DetElementTypes {
      TOP     = 1 << 0,
      STATION = 1 << 1,
      LAYER   = 1 << 2,
      QUARTER = 1 << 3,
      MODULE  = 1 << 4,
      MAT     = 1 << 5
    };
    constexpr uint32_t nStations      = 3; // TODO Detector#54 {dd4hep::_toInt( "FT:nStations" )};
    constexpr uint32_t nLayers        = 4; // TODO Detector#54 {dd4hep::_toInt( "FT:nLayers" )};
    constexpr uint32_t nLayersTotal   = nStations * nLayers;
    constexpr uint32_t nXLayersTotal  = nStations * 2;
    constexpr uint32_t nUVLayersTotal = nStations * 2;

    constexpr uint32_t nSides                = 2;
    constexpr uint32_t nZones                = 2;
    constexpr uint32_t nHalfLayers           = 2; // TODO Detector#54 {dd4hep::_toInt( "FT:nHalfLayers" )};
    constexpr uint32_t nQuarters             = 4; // TODO Detector#54 {dd4hep::_toInt( "FT:nQuarters" )};
    constexpr uint32_t nQuartersPerHalfLayer = 2; // TODO Detector#54 {dd4hep::_toInt( "FT:nStations" )};
    constexpr uint32_t nModulesMax           = 6; // TODO Detector#54 {dd4hep::_toInt( "?" )};
    constexpr uint32_t nMats                 = 4; // TODO Detector#54 {dd4hep::_toInt( "FT:nMats" )};
    constexpr uint32_t nHalfRobs             = 2;
    constexpr uint32_t nLinksPerClusterBoard = 2;
    constexpr uint32_t nLinksMaxPerTell40    = 24;
    constexpr uint32_t nSiPM                 = 4;   // TODO Detector#54 {dd4hep::_toInt( "FT:nSiPMs" )};
    constexpr uint32_t nChannels             = 128; // TODO Detector#54 {dd4hep::_toInt( "FT:nChannelsInSiPM" )};
    constexpr uint32_t nDie                  = 2;
    constexpr uint32_t nChannelsInDie        = nChannels / nDie;
    constexpr uint32_t nSiPMsPerModule       = nSiPM * nMats;
    constexpr uint32_t nChannelsPerModule    = nSiPMsPerModule * nChannels;
    constexpr uint32_t nQuartersStation      = nQuarters * nLayers;
    constexpr uint32_t nQuartersTotal        = nQuartersStation * nStations;
    constexpr uint32_t nZonesTotal           = nQuartersTotal / 2;
    constexpr uint32_t nMatsMax              = ( nStations + 1 ) * ( nQuartersStation ) * ( nModulesMax ) *
                                  ( nMats ); // maximum number of mats from naive calculation
    constexpr uint32_t nModulesTotal          = nQuartersTotal * 5 + nLayers * nQuarters;
    constexpr uint32_t nMatsTotal             = nMats * nModulesTotal;
    constexpr uint16_t nSiPMsTotal            = nSiPMsPerModule * nModulesTotal;
    constexpr uint16_t nMaxMatsPerQuarter     = nMats * nModulesMax;
    constexpr uint16_t nMaxSiPMsPerQuarter    = nSiPM * nMaxMatsPerQuarter;
    constexpr uint16_t nMaxChannelsPerQuarter = nChannels * nMaxSiPMsPerQuarter;

    constexpr inline std::size_t maxNumberMats  = 2 * nLayersTotal * nModulesMax * nSiPMsPerModule; // 2304
    constexpr inline float       triangleHeight = 22.7f;                                            // mm

    constexpr inline auto xLayers      = std::array{0, 3, 4, 7, 8, 11};
    constexpr inline auto stereoLayers = std::array{1, 2, 5, 6, 9, 10};
    static_assert( xLayers.size() == nXLayersTotal && "Not enough or too many X layers declared" );
    static_assert( stereoLayers.size() == nUVLayersTotal && "Not enough or too many stereo layers declared" );
    static_assert( xLayers.size() + stereoLayers.size() == nLayersTotal && "All layers must be either X or UV" );

    // Defining zones. UpperZone is 1, LowerZone is 0. TODO: This could be strong-typed

    constexpr inline auto makeXLayerArray = []( unsigned int zone ) {
      auto arr = xLayers;
      std::transform( xLayers.begin(), xLayers.end(), arr.begin(), [&zone]( auto i ) { return 2 * i + zone; } );
      return arr;
    };
    constexpr inline auto xZonesUpper = makeXLayerArray( 1 ); //{1, 7, 9, 15, 17, 23};
    constexpr inline auto xZonesLower = makeXLayerArray( 0 ); //{0, 6, 8, 14, 16, 22};

    constexpr inline auto makeUVLayerArray = []( unsigned int zone ) {
      auto arr = stereoLayers;
      std::transform( stereoLayers.begin(), stereoLayers.end(), arr.begin(),
                      [&zone]( auto i ) { return 2 * i + zone; } );
      return arr;
    };
    constexpr inline auto uvZonesUpper = makeUVLayerArray( 1 ); //{3, 5, 11, 13, 19, 21};
    constexpr inline auto uvZonesLower = makeUVLayerArray( 0 ); //{2, 4, 10, 12, 18, 20};

    constexpr inline auto ZonesLower = []() {
      std::array<unsigned int, nLayersTotal> arr;
      for ( unsigned int iLayer = 0; iLayer < nLayersTotal; iLayer++ ) { arr[iLayer] = 2 * iLayer; }
      return arr;
    }(); //{0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22};

    constexpr inline auto stereoZones = []() {
      std::array<unsigned int, nUVLayersTotal * 2> arr;
      for ( unsigned int iLayer = 0; iLayer < nUVLayersTotal; iLayer++ ) {
        arr[2 * iLayer]     = uvZonesLower[iLayer];
        arr[2 * iLayer + 1] = uvZonesUpper[iLayer];
      }
      return arr;
    }();

    //    constexpr inline auto xStereoLayers = std::array{0, 3, 4, 7, 8, 11, 1, 2, 5, 6, 9, 10};
    constexpr inline auto isXLayer =
        std::array{true, false, false, true, true, false, false, true, true, false, false, true};

    enum UpperZones {
      T1X1 = xZonesUpper[0],
      T1U  = uvZonesUpper[0],
      T1V  = uvZonesUpper[1],
      T1X2 = xZonesUpper[1],
      T2X1 = xZonesUpper[2],
      T2U  = uvZonesUpper[2],
      T2V  = uvZonesUpper[3],
      T2X2 = xZonesUpper[3],
      T3X1 = xZonesUpper[4],
      T3U  = uvZonesUpper[4],
      T3V  = uvZonesUpper[5],
      T3X2 = xZonesUpper[5],
    };

    // Shifts. TODO: remove Detector!442
    constexpr uint16_t stationShift = 1;
    constexpr uint16_t layerShift   = stationShift * nLayers;
    constexpr uint16_t quarterShift = layerShift * nQuarters;

    /**
     * the maximum number of possible hits in the SciFiTracker is a hardware limit determined by the
     * output bandwidth of the TELL40. The max number is 45568, round it up to 50k.
     * (for more details ask Sevda Esen and Olivier Le Dortz)
     */
    constexpr inline std::size_t maxNumberHits{50000};

    // Others
    //---LoH: Detector!442 change
    constexpr uint16_t nModulesPerQuarter( const uint16_t station ) { return station < 3 ? 5u : 6u; }
    constexpr uint16_t nModulesPerStation( const uint16_t station ) {
      return nModulesPerQuarter( station ) * nQuarters * nLayers;
    }
    namespace GeomInfo {
      //---LoH: check that these are not used anywhere else
      constexpr double z_midT1 = 7931.0; // mm
      constexpr double z_midT2 = 8613.0; // mm
      constexpr double z_midT3 = 9298.0; // mm
    };                                   // namespace GeomInfo
  }                                      // namespace FT

} // End namespace LHCb::Detector
