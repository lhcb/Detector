/*****************************************************************************\
 * (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// All constants related to the FT data format
#include "FTConstants.h"

namespace LHCb::Detector {
  namespace FT {
    namespace RawBank {
      // Data format is taken from https://edms.cern.ch/ui/file/2100937/5/edms_2100937_raw_data_format_run3.pdf
      // and https://edms.cern.ch/ui/file/1904563/5.8/Scifi_TELL40_Data_Processing_5.8.pdf

      // Hardware limits
      inline constexpr uint16_t maxClusterWidth = 4;
      inline constexpr uint16_t nbClusMaximum   = 31;  // 5 bits
      inline constexpr uint16_t nbDigitMaximum  = 128; // 128 digits per link
      inline constexpr uint16_t nbClusFFMaximum = 10;  //
      inline constexpr uint16_t nbClusFVMaximum = 16;  //
      inline constexpr uint16_t maxPseudoSize   = 16;  // Important thing is that it is larger than 8
      inline constexpr uint16_t maxAdcCharge    = 4;   // Important thing is that it is larger than 3

      // Bank properties
      enum BankProperties {
        NbBanksPerQuarter  = 5,
        NbLinksPerBank     = 24,
        NbBanksMax         = NbBanksPerQuarter * nQuarters * nLayersTotal,
        NbLinksMax         = NbLinksPerBank * NbBanksMax,
        NbClusMaxInBank    = nbClusMaximum * NbLinksPerBank,
        NbClusMaxInQuarter = nbClusMaximum * NbLinksPerBank * NbBanksPerQuarter
      };

      // Objects that describe the data content
      using type_clusterData = uint16_t;
      enum struct FTClusterData : type_clusterData {};
      //---LoH: Exclusively true for decoding version != 0
      using FTClusterWord              = uint32_t;
      constexpr int nClustersForHeader = sizeof( FTClusterWord ) / sizeof( FTClusterData );

      // Data format
      constexpr uint32_t leftHeader  = 0b00111;
      constexpr uint32_t rightHeader = 0b01000;
      enum allocatedBits { size = 1, fraction = 1, channel = 7, link = 6 };
      // There are unused bits, and link index could be reduced to 5 actually, so 4 bits could be added.
      static_assert( ( allocatedBits::size + allocatedBits::fraction + allocatedBits::channel + allocatedBits::link ) <
                     sizeof( FTClusterData ) * CHAR_BIT );
      constexpr uint32_t clusterLength =
          allocatedBits::channel + allocatedBits::fraction + allocatedBits::size; // Channel (7) Frac (1) Size (1)
      enum shifts {
        sizeShift     = 0,
        fractionShift = allocatedBits::size,                                                    // 1
        channelShift  = allocatedBits::size + allocatedBits::fraction,                          // 2
        linkShift     = allocatedBits::size + allocatedBits::fraction + allocatedBits::channel, // 9
      };

      enum masks {
        linkMask     = BIT_MASK( type_clusterData, allocatedBits::link ),
        channelMask  = BIT_MASK( type_clusterData, allocatedBits::channel ),
        fractionMask = BIT_MASK( type_clusterData, allocatedBits::fraction ),
        sizeMask     = BIT_MASK( type_clusterData, allocatedBits::size ),
      };
      // Check that the data format is consistent with the rest
      static_assert( ( channelMask >= nChannels - 1 ) && "There must be enough bits to encode the channels." );
      static_assert( ( linkMask >= nLinksMaxPerTell40 - 1 ) && "There must be enough bits to encode the links." );
      static_assert( ( static_cast<type_clusterData>( masks::linkMask ) >=
                       static_cast<type_clusterData>( BankProperties::NbLinksPerBank - 1 ) ) &&
                     "There needs to be enough bits to encode the maximum number of links." );

      static_assert( ( nClustersForHeader == 2 ) &&
                     "Logic of the raw bank encoder is hardcoded for 2 clusters per word, same for decoder" );
      static_assert( ( ( sizeof( FTClusterData ) * CHAR_BIT ) >= clusterLength ) &&
                     "FTClusterData must be large enough to contain all the cluster information" );

      // Useful methods to manipulate FTClusterData
      [[nodiscard]] constexpr FTClusterWord to_word( FTClusterData c ) { return static_cast<FTClusterWord>( c ); }
      [[nodiscard]] inline constexpr auto   addToWord( FTClusterWord w, FTClusterData c ) {
        return w | ( static_cast<FTClusterWord>( c ) << CHAR_BIT * sizeof( FTClusterData ) );
      };

      [[nodiscard]] constexpr unsigned linkIdx( FTClusterData c ) {
        return ( ( static_cast<type_clusterData>( c ) >> linkShift ) & linkMask );
      }
      [[nodiscard]] constexpr int channelInSiPM( FTClusterData c ) {
        return ( ( static_cast<type_clusterData>( c ) >> channelShift ) & channelMask );
      }
      [[nodiscard]] constexpr int channelInBank( FTClusterData c ) {
        return ( ( static_cast<type_clusterData>( c ) >> channelShift ) );
      }
      [[nodiscard]] constexpr int fracBit( FTClusterData c ) {
        return ( ( static_cast<type_clusterData>( c ) >> fractionShift ) & fractionMask );
      }
      [[nodiscard]] constexpr bool sizeBit( FTClusterData c ) {
        return ( ( static_cast<type_clusterData>( c ) >> sizeShift ) & sizeMask );
      }
      constexpr bool isNullCluster( FTClusterData c ) { return static_cast<type_clusterData>( c ) == 0; }

      // Functions to treat clusters

      template <unsigned int vrsn>
      bool begCluster( const FTClusterData c ) {
        switch ( vrsn ) {
        case 5:
          return true;
        case 7:
          // Contrary to v8, size 4 means that the first fragment of a large cluster always has fraction 0
          // but this is not by design
          return sizeBit( c ) && !fracBit( c );
        case 6:
        case 8:
          return sizeBit( c ) && fracBit( c );
        default:
          return false;
        }
      }

      template <unsigned int vrsn>
      bool endCluster( const FTClusterData c ) {
        switch ( vrsn ) {
        case 5:
        case 7:
          return sizeBit( c );
        case 6:
        case 8:
          return sizeBit( c ) && !fracBit( c );
        default:
          return false;
        }
      }
    } // namespace RawBank
  }   // namespace FT
} // namespace LHCb::Detector
