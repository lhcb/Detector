/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Detector/Muon/Types.h>

#if __has_include( <catch2/catch.hpp>)
// Catch2 v2
#  include <catch2/catch.hpp>
namespace Catch {
  using Detail::Approx;
}
#else
// Catch2 v3
#  include <catch2/catch_all.hpp>
#  include <catch2/catch_test_macros.hpp>
#endif

using namespace LHCb::Detector::Muon;

TEST_CASE( "Types" ) {
  StationId s = StationN{3};
  CHECK( s.value() == 1 );
  CHECK( StationN{s}.value() == 3 );
}
