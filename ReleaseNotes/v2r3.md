2025-02-13 Detector v2r3
===

This version uses LCG [105c](http://lcginfo.cern.ch/release/105c/)

This version is released on the `master` branch.
Built relative to Detector [v2r2](/../../tags/v2r2), with the following changes:

- ~VP | Expose in DeVP the global system alignment translation and rotation, !670 (@tlatham) [#70]
- ~VP | Set VP to use simulation conditions properly, !374 (@hcroft)
- ~RICH | Update RICH1 with survey parameters for 2025 detector version, !667 (@jonrob)
- Update the definition of nbEcalCells, !673 (@zejia)
- Update namespace for TVConstants, !655 (@juarudol)
- Add global coordinate transformations for offline calibration to detector, !643 (@ldufour)
