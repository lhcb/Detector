2024-09-29 Detector v1r36
===

This version uses LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.0.

This version is released on the `2024-patches` branch.
Built relative to Detector [v1r35](/../../tags/v1r35), with the following changes:

- ~VP | Modify VP 2024.Q3.4 geometry to still include the 0.5mm shims and rename it 2024.v00.00, !577 (@tlatham)
