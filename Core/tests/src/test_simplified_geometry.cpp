/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Core/DetectorDataService.h>
#include <Core/Keys.h>
#include <DD4hep/Detector.h>
#include <Detector/LHCb/DeLHCb.h>
#include <Detector/Test/Fixture.h>
#include <cstdio>

#if __has_include( <catch2/catch.hpp>)
// Catch2 v2
#  include <catch2/catch.hpp>
namespace Catch {
  using Detail::Approx;
}
#  define ContainsSubstring Contains
#else
// Catch2 v3
#  include <catch2/catch_all.hpp>
#endif

TEST_CASE( "simplified_geometry" ) {

  Detector::Test::Fixture f;

  auto& description = f.description();
  description.fromXML( "compact/run3/trunk/LHCb.xml", dd4hep::DetectorBuildType::BUILD_DISPLAY );

  REQUIRE( description.state() == dd4hep::Detector::READY );

  auto det = description.detector( "/world" );
  // the `!!` is needed because handles have `operator!` but not `operator bool`
  REQUIRE( !!det );

  LHCb::Detector::DetectorDataService dds( description, {"/world"} );
  dds.initialize( nlohmann::json( {{"repository", "file:tests/ConditionsIOV"}} ) );

  // Without fibres in the ECAL we have less than 1.5 million nodes...
  dd4hep::printout( dd4hep::INFO, "TestSimplifiedCalo",
                    ( fmt::format( "Found {:5d} nodes in the geometry\n", description.manager().CountNodes() ) ) );
  REQUIRE( description.manager().CountNodes() < 1500000 );
}
