/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#if __has_include( <catch2/catch.hpp>)
// Catch2 v2
#  include <catch2/catch.hpp>
namespace Catch {
  using Detail::Approx;
}
#else
// Catch2 v3
#  include <catch2/catch_all.hpp>
#endif
#include <cmath>
#include <exception>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include <DD4hep/ConditionsPrinter.h>
#include <DD4hep/ConditionsProcessor.h>
#include <DD4hep/Detector.h>
#include <DD4hep/Factories.h>
#include <DD4hep/Printout.h>

#include <Core/DetectorDataService.h>
#include <Core/Keys.h>

struct Alignment {
  TGeoPhysicalNode phys_node;
  TGeoHMatrix      delta_matrix;

  Alignment( std::string placement_path, const dd4hep::Delta& alignment_delta ) : phys_node{placement_path.c_str()} {
    alignment_delta.computeMatrix( delta_matrix );
    TGeoMatrix* mm = phys_node.GetNode()->GetMatrix();
    delta_matrix.MultiplyLeft( mm ); // orig * delta
    delta_matrix.Print();
    phys_node.Align( &delta_matrix );
  }
};

struct Geometry {

  Geometry( dd4hep::Detector& description, int slice = 100 ) : m_dds( description, {"/world", "scope"} ) {
    // Detector initialization, including loading the condition slice
    const char* compact_path    = "tests/testscope/scope.xml";
    const char* conditions_path = "file:tests/testscope/";
    description.fromXML( compact_path );
    m_dds.initialize( nlohmann::json{{"repository", conditions_path}} );
    m_slice = m_dds.get_slice( slice );
  }

  ~Geometry() { m_dds.finalize(); }

  LHCb::Detector::DetectorDataService                     m_dds;
  LHCb::Detector::DetectorDataService::ConditionsSlicePtr m_slice;
  std::map<std::string, std::unique_ptr<Alignment>>       m_physical_nodes;
};

void _iterate( dd4hep::DetElement det, Geometry& geometry ) {
  std::cout << "===> " << det.path() << " : " << det.placementPath() << '\n';
  auto cond = geometry.m_slice->get( det, LHCb::Detector::Keys::deltaKey );
  if ( cond.isValid() ) {
    const dd4hep::Delta& d = cond.get<dd4hep::Delta>();
    std::cout << "Delta: " << d << '\n';
    geometry.m_physical_nodes[det.path()] = std::make_unique<Alignment>( det.placementPath(), d );
  } else {
    std::cout << "Delta cond not valid!\n";
  }
  for ( auto child : det.children() ) { _iterate( child.second, geometry ); }
}

int _traverse( TGeoNavigator* nav, ROOT::Math::XYZPoint initial, ROOT::Math::XYZPoint final ) {

  // Traversing the volumes using the navigators
  int sensor_count = 0;
  std::cout << "\nChecking the materials found between " << initial << " and " << final << '\n';
  nav->SetCurrentPoint( initial.X(), initial.Y(), initial.Z() );
  auto direction = ( final - initial ).Unit();
  nav->SetCurrentDirection( direction.X(), direction.Y(), direction.Z() );
  nav->FindNode( initial.X(), initial.Y(), initial.Z() );
  const char* matname = nav->GetCurrentNode()->GetVolume()->GetMaterial()->GetName();
  std::cout << initial << " - " << nav->GetCurrentNode()->GetName() << " - " << matname << '\n';
  double dist2_final = ( final - initial ).R();
  double dist2       = 0;
  do {
    nav->FindNextBoundaryAndStep();
    auto c = nav->GetCurrentPoint();

    ROOT::Math::XYZPoint current( c[0], c[1], c[2] );
    matname = nav->GetCurrentNode()->GetVolume()->GetMaterial()->GetName();
    std::cout << current << " - " << nav->GetPath() << " - " << matname << '\n';
    if ( strcmp( matname, "Silicon" ) == 0 ) { sensor_count++; }
    dist2 = ( current - final ).R();
  } while ( dist2 < dist2_final );
  return sensor_count;
}

TEST_CASE( "global_alignment" ) {

  dd4hep::Detector& description = dd4hep::Detector::getInstance();
  Geometry          geom{dd4hep::Detector::getInstance()};

  REQUIRE( geom.m_slice );

  // Printing the conditions for the detector element
  dd4hep::cond::ConditionsPrinter printer( geom.m_slice.get(), "",
                                           dd4hep::Condition::WITH_IOV | dd4hep::Condition::WITH_ADDRESS |
                                               dd4hep::Condition::WITH_TYPE | dd4hep::Condition::WITH_COMMENT |
                                               dd4hep::Condition::WITH_DATATYPE );
  printer( description.detector( "/world/scope/side1/sensor0" ), 0 );

  // Getting the DeIOV object
  dd4hep::DetElement          side1sensor0Det = description.detector( "/world/scope/side1/sensor0" );
  const LHCb::Detector::DeIOV side1sensor0    = geom.m_slice->get( side1sensor0Det, LHCb::Detector::Keys::deKey );
  REQUIRE( side1sensor0.isValid() );

  // Traversing side 1 to check the positions
  ROOT::Math::XYZPoint start{-22, 0, -600};
  ROOT::Math::XYZPoint end{-22, 0, 600};
  int                  sensor_count = _traverse( gGeoManager->GetCurrentNavigator(), start, end );
  std::cout << "Found " << sensor_count << " sensors between " << start << " and " << end << '\n';
  // We should cross all 10 sensors if they are perfectly aligned, as they are 20mm from the center of the box
  REQUIRE( sensor_count == 10 );
  // gGeoManager->Export("geo1.root");

  // Now we misalign the subdetectors and lock the positions
  auto world = description.world();
  _iterate( world, geom );
  gGeoManager->RefreshPhysicalNodes( kTRUE );
  // gGeoManager->Export("geo2.root");

  // And traverse again
  sensor_count = _traverse( gGeoManager->GetCurrentNavigator(), start, end );
  std::cout << "Found " << sensor_count << " sensors between " << start << " and " << end << '\n';
  REQUIRE( sensor_count == 9 );
}
