/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#if __has_include( <catch2/catch.hpp>)
// Catch2 v2
#  include <catch2/catch.hpp>
namespace Catch {
  using Detail::Approx;
}
#else
// Catch2 v3
#  include <catch2/catch_all.hpp>
#endif
#include <cmath>
#include <exception>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include <DD4hep/ConditionsPrinter.h>
#include <DD4hep/ConditionsProcessor.h>
#include <DD4hep/Detector.h>
#include <DD4hep/Factories.h>
#include <DD4hep/Printout.h>

#include <Core/DetectorDataService.h>
#include <Core/Keys.h>

std::mutex navigation_mutex;

void test_navigation( LHCb::Detector::DeIOV det ) {

  // const std::lock_guard<std::mutex> lock( navigation_mutex );
  ROOT::Math::XYZPoint det_center_pos = det.toGlobal( ROOT::Math::XYZPoint( 0, 0, 0 ) );

  // Navigation test
  const double SCAN_SIZE     = 20.0;
  const double STEP          = 0.2;
  int          points_total  = 0;
  int          points_inside = 0;
  for ( double x = det_center_pos.X() - SCAN_SIZE; x <= det_center_pos.X() + SCAN_SIZE; x += STEP ) {
    for ( double y = det_center_pos.Y() - SCAN_SIZE; y <= det_center_pos.Y() + SCAN_SIZE; y += STEP ) {
      for ( double z = det_center_pos.Z() - SCAN_SIZE; z <= det_center_pos.Z() + SCAN_SIZE; z += STEP ) {
        bool inside = false;
        { inside = det.isInside( ROOT::Math::XYZPoint( x, y, z ) ); }
        points_total++;
        if ( inside ) points_inside++;
      }
    }
  }
  std::cout << "Total: " << points_total << ", Inside:" << points_inside << '\n';
  REQUIRE( points_total == 8080200 );
  REQUIRE( points_inside == 603000 );
}

struct Geometry {

  Geometry( dd4hep::Detector& description, int slice = 100 ) : m_dds( description, {"/world", "scope"} ) {
    // Detector initialization, including loading the condition slice
    const char* compact_path    = "tests/testscope/scope.xml";
    const char* conditions_path = "file:tests/testscope/";
    description.fromXML( compact_path );
    m_dds.initialize( nlohmann::json{{"repository", conditions_path}} );
    m_slice = m_dds.get_slice( slice );
  }

  ~Geometry() { m_dds.finalize(); }

  LHCb::Detector::DetectorDataService                     m_dds;
  LHCb::Detector::DetectorDataService::ConditionsSlicePtr m_slice;
};

TEST_CASE( "navigation_test" ) {

  dd4hep::Detector& description = dd4hep::Detector::getInstance();
  Geometry          geom{dd4hep::Detector::getInstance()};

  REQUIRE( geom.m_slice );

  // Printing the conditions for the detector element
  dd4hep::cond::ConditionsPrinter printer( geom.m_slice.get(), "",
                                           dd4hep::Condition::WITH_IOV | dd4hep::Condition::WITH_ADDRESS |
                                               dd4hep::Condition::WITH_TYPE | dd4hep::Condition::WITH_COMMENT |
                                               dd4hep::Condition::WITH_DATATYPE );
  printer( description.detector( "/world/scope/side1/sensor0" ), 0 );

  // Getting the DeIOV object
  dd4hep::DetElement          side1sensor0Det = description.detector( "/world/scope/side1/sensor0" );
  const LHCb::Detector::DeIOV side1sensor0    = geom.m_slice->get( side1sensor0Det, LHCb::Detector::Keys::deKey );
  REQUIRE( side1sensor0.isValid() );

  // Testing the isInside method
  test_navigation( side1sensor0 );
}

// Running the same method with several threads
TEST_CASE( "navigation_test_MT" ) {

  dd4hep::Detector& description = dd4hep::Detector::getInstance();
  Geometry          geom{dd4hep::Detector::getInstance()};
  REQUIRE( geom.m_slice );

  // Getting the DeIOV object
  dd4hep::DetElement          side1sensor0Det = description.detector( "/world/scope/side1/sensor0" );
  const LHCb::Detector::DeIOV side1sensor0    = geom.m_slice->get( side1sensor0Det, LHCb::Detector::Keys::deKey );
  REQUIRE( side1sensor0.isValid() );

  // Testing the isInside method

  auto runtest = [side1sensor0] {
    for ( int i = 1; i <= 3; i++ ) {
      std::cout << std::this_thread::get_id() << " iteration " << i << " starting\n";
      test_navigation( side1sensor0 );
      std::cout << std::this_thread::get_id() << " iteration " << i << " done\n";
    }
  };
  std::vector<std::thread> threads;
  for ( int i = 1; i <= 20; i++ ) { threads.push_back( std::thread{runtest} ); }
  for ( auto& th : threads ) { th.join(); }
}
