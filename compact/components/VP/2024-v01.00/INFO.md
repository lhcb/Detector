## VP version 2024-v01.00

VP geometry for the whole of 2024

Shim size is set to 0.5mm

Originally, the 0.5mm shims were intended to be removed during TS1 at the start of June.
Hence the original VP geometry version with those shims was named `2024.Q1.2-v00.00` and another version was prepared without the shims and named `2024.Q3.4-v00.00`.
However, following the decision not to remove the shims during TS1, the VP geometry is actually unchanged during 2024.

With respect to 2024-v00.00, this new version includes the updates added in:
* [!616](https://gitlab.cern.ch/lhcb/Detector/-/merge_requests/616) New models for the RF box and downstream WF suppressor
* [!374](https://gitlab.cern.ch/lhcb/Detector/-/merge_requests/374) and [!544](https://gitlab.cern.ch/lhcb/Detector/-/merge_requests/544) Add optional simulation conditions
