## VP version 2025-v00.00

VP geometry for the whole of 2025

Shims are removed following their actual removal during the YETS.
Otherwise this version is identical to 2024-v01.00
