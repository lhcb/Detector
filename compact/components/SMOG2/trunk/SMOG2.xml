<!--
    (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<lccdd>

  <info name="SMOG2" title="SMOG2" author="Marco Santimaria" url="https://cds.cern.ch/record/2673690" status="development" version="$Id: $">
    <comment>
      SMOG2 gas storage cell
    </comment>
  </info>

  <define>
    <constant name="SMOG2:tube_rmin" value="5*mm" />
    <constant name="SMOG2:tube_thickness" value="0.2*mm" />
    <constant name="SMOG2:tube_length" value="20*cm" />
    <constant name="SMOG2:supp_rmin" value="13*mm" />
    <constant name="SMOG2:supp_rmax" value="17*mm" />
    <constant name="SMOG2:supp_thickness" value="5*mm" />
    <constant name="SMOG2:supp_dist" value="1*cm" />
    <constant name="SMOG2:cone_rmax" value="28*mm" />
    <constant name="SMOG2:cone_length" value="8*cm" />
    <constant name="SMOG2:ring_dr" value="5*mm" />
    <constant name="SMOG2:wing_dx" value="48.3*mm" />
    <constant name="SMOG2:wing_dy" value="1.2*mm" />
    <constant name="SMOG2:supp_h" value="1*cm" />
    <constant name="SMOG2:bar_dim" value="1*cm" />
    <constant name="SMOG2:wing_delta" value="0*mm" />
    <constant name="SMOG2:delta_z" value="0.1*mm" />
    <constant name="SMOG2:wfs_length" value="98.5*mm" />
    <constant name="SMOG2:wfs_down_length" value="3.6*mm" />
    <constant name="SMOG2:wfs_hole_r" value="2.5*mm" />
    <constant name="SMOG2:wfs_clear" value="5*mm" />
    <constant name="SMOG2:opening_shift" value="0.5*mm" />
    <constant name="SMOG2:h_sub" value="4*cm"/>
    <constant name="SMOG2:pistS_r" value="1.5*mm"/>
    <constant name="SMOG2:pistS_length" value="13*mm"/>
    <constant name="SMOG2:pistB_r" value="7*mm"/>
    <constant name="SMOG2:pistB_length" value="15*mm"/>
    <constant name="SMOG2:fixed_tol" value="0*mm"/>
    <constant name="SMOG2:z_pos" value="VP:RFBoxZPosition - VP:RFBoxTopPlateZSize/2 - VP:RFBoxThick - SMOG2:tube_length/2 - SMOG2:wfs_down_length - 2*SMOG2:wing_dy - 2*mm"/>
  </define>

  <display>
    <vis name="SMOG2:CellVis" alpha="0.3" showDaughters="true"  visible="false"/>
    <vis name="SMOG2:VacuumVis" alpha="1.0" r="0.2" g="0.2" b="0.2" showDaughters="true" visible="true" />
    <vis name="SMOG2:AlVis" alpha="1.0" r="0.75" g="0.75" b="0.75" showDaughters="true" visible="true" />
    <vis name="SMOG2:WingVis" alpha="0.6" r="0.16" g="0.30" b="0.75" showDaughters="true" visible="true" />
    <vis name="SMOG2:WingTVis" alpha="0.6" r="0.31" g="0.44" b="0.85" showDaughters="true" visible="true" />
    <vis name="SMOG2:RingVis" alpha="1.0" r="0.95" g="0.23" b="0.05" showDaughters="true" visible="true" />
    <vis name="SMOG2:WFSVis" alpha="1.0" r="1.0" g="0.65" b="0" showDaughters="true" visible="true" />
    <vis name="SMOG2:SuppVis" alpha="0.2" r="0" g="0.5" b="0" showDaughters="true" visible="true" />
  </display>

  <detectors>

      <detector id="SMOG2_L:ID" name="SMOG2_L" type="LHCb_SMOG2_Left_v1_0" parent="${SMOG2_L:parent}" vis="CellVis">
      <tube rmin="SMOG2:tube_rmin" thickness="SMOG2:tube_thickness" length="SMOG2:tube_length"/>
      <cone rmax="SMOG2:cone_rmax" length="SMOG2:cone_length"/>
      <box dx="SMOG2:wing_dx" dy="SMOG2:wing_dy" dz="SMOG2:tube_length"/>
      <position x="SMOG2:opening_shift" y="0*cm" z="SMOG2:z_pos"/>
      <rotation z="0*degree" y = "0*degree" x="180*degree"/>
    </detector>

      <detector id="SMOG2_R:ID" name="SMOG2_R" type="LHCb_SMOG2_Right_v1_0" parent="${SMOG2_R:parent}" vis="CellVis">
      <tube rmin="SMOG2:tube_rmin" thickness="SMOG2:tube_thickness" length="SMOG2:tube_length"/>
      <cone rmax="SMOG2:cone_rmax" length="SMOG2:cone_length"/>
      <box dx="SMOG2:wing_dx" dy="SMOG2:wing_dy" dz="SMOG2:tube_length"/>
      <position x="-SMOG2:opening_shift" y="0*cm" z="SMOG2:z_pos"/>
      <rotation z="180*degree" y = "0*degree" x="180*degree"/>
    </detector>

      <detector id="SMOG2_F:ID" name="SMOG2_F" type="LHCb_SMOG2_Fixed_v1_0" parent="${SMOG2_F:parent}" vis="CellVis">
      <cone rmax="SMOG2:cone_rmax" length="SMOG2:cone_length"/>
      <box dx="SMOG2:wing_dx" dy="SMOG2:wing_dy" dz="SMOG2:tube_length"/>
      <position x="0*cm" y="0*cm" z="SMOG2:z_pos - SMOG2:tube_length/2 - SMOG2:cone_length - SMOG2:wing_dy - SMOG2:fixed_tol"/>
      <rotation z="0*degree" y = "0*degree" x="180*degree"/>
    </detector>

  </detectors>

  <!--
  <plugins>
    <plugin name="LHCb_Align_cond_XML_reader" type="xml"/>
  </plugins>
  -->

</lccdd>
